;( function( window ) {

  'use strict';
    
  /**
   * Extend obj function
   *
   * This is an object extender function. It allows us to extend an object
   * by passing in additional variables and overwriting the defaults.
   */
  function extend( a, b ) {
    for( var key in b ) { 
      if( b.hasOwnProperty( key ) ) {
        a[key] = b[key];
      }
    }
    return a;
  };
    

  /**
   * PKBrushUnMasker
   */
  function PKBrushUnMasker( options ) {
    // function body...
     this.options = extend( {}, this.options );
     extend( this.options, options ); 
     this._init(); 
  };
    
   /**
   * PKBrushUnMasker options Object
   *
   * @type {HTMLElement} wrapper - The wrapper to create the ray in.
   */
  PKBrushUnMasker.prototype.options = {
      //fill: document.body,
      brush: document.body,
      canv: document.body,
      w: 300,
      h: 250,
      cols: 3,
      rows: 3,
      ow: 300,
      oh: 250
  };

  PKBrushUnMasker.prototype._init = function() {
      //this.fill = this.options.fill;
      //TweenLite.set(this.fill,{alpha:0});
      
      this.brush = this.options.brush;
      TweenLite.set(this.brush,{alpha:0});
      
      this.canv = this.options.canv;
      this.ctx = this.canv.getContext('2d');
      
      this.totalframes = this.options.cols * this.options.rows;
      
      this.frame = 1;
      this.update();
    };
    
    /**
     * PKBrushUnMasker show
     *
     * This function starts our ray moving.
     * assumes GSAP.
     */
    PKBrushUnMasker.prototype.show = function() {
        TweenLite.ticker.addEventListener("tick", this.update, this);
    };
                                      
    PKBrushUnMasker.prototype.update = function() {
      if (this.frame % 3 == 0) {
        var offset = this.totalframes - (this.frame / 3);
        var dx = offset % this.options.cols * this.options.w;
        var dy = Math.floor(offset / this.options.cols) * this.options.h;
        this.ctx.clearRect(0, 0, this.options.w, this.options.h);
        this.ctx.globalCompositeOperation = "source-over";
        this.ctx.drawImage(this.brush, dx, dy, this.options.w, this.options.h, 0, 0, this.options.ow, this.options.oh);
        //this.ctx.globalCompositeOperation = "source-atop";
        //this.ctx.drawImage(this.fill, -this.options.offsetx, -this.options.offsety);
        if (offset == 0) {
          TweenLite.ticker.removeEventListener("tick", this.update);
        }
      }
      this.frame++;
    };
    
  /**
   * Add to global namespace
   */
  window.PKBrushUnMasker = PKBrushUnMasker;

})( window );